export LANG='en_US.UTF-8'
export LC_COLLATE='C'

export VISUAL='vim'
export EDITOR="${VISUAL} -e"

export PAGER='less'
export LESS='-cimr'
