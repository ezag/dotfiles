" greenz.vim - vim color file

highlight clear
set background=dark
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "greenz"

" General
hi Normal        ctermfg=none        ctermbg=none        cterm=none
hi Normal        guifg=#50aa40       guibg=#000000       gui=none
hi Cursor        ctermfg=black       ctermbg=white       cterm=none
hi Cursor        guifg=#000000       guibg=#ffffff       gui=none
hi lCursor       ctermfg=black       ctermbg=Cyan        cterm=none
hi lCursor       guifg=#000000       guibg=#00dddd       gui=none
hi CursorLine                        ctermbg=none        cterm=none
hi CursorLine                        guibg=#0c1e09       gui=none
hi LineNr        ctermfg=grey                        cterm=none
hi LineNr        guifg=#808080       guibg=#121212       gui=none
hi Visual                                                cterm=none
hi Visual                            guibg=#104216       gui=none

" Interface
hi ModeMsg       ctermfg=white                           cterm=none
hi ModeMsg       guifg=#ffffff                           gui=none
hi TabLine       guifg=#808080 guibg=#121212
hi TabLineSel    guifg=#ffffff guibg=#000000   gui=none
hi TabLineFill   guifg=#121212

" Language
hi Keyword      ctermfg=green                            cterm=none
hi Keyword      guifg=#cccc00                            gui=none
hi Operator     ctermfg=green                            cterm=none
hi Operator     guifg=#00ff00                            gui=none
hi Type         guifg=yellow                             cterm=none
hi Type         guifg=#00ff00                            gui=none
hi Structure    ctermfg=yellow                           cterm=none
hi Structure    guifg=#ffff00                            gui=none
hi PreProc      ctermfg=brown                            cterm=none
hi PreProc      guifg=#7f7f00                            gui=none
hi Identifier   ctermfg=grey                             cterm=none
hi Identifier   guifg=#00ff88                            gui=none
hi Comment      ctermfg=grey                         cterm=none
hi Comment      guifg=#888888                            gui=none
hi Number       ctermfg=cyan                             cterm=none
hi Number       guifg=#bbccdd                            gui=none
hi Character    ctermfg=darkcyan                         cterm=none
hi Character    guifg=#ddddcc                            gui=none
hi String       ctermfg=darkcyan                         cterm=none
hi String       guifg=#ccccaa                            gui=none
hi MatchParen   ctermfg=white        ctermbg=none        cterm=none
hi MatchParen   guifg=#ffffff        guibg=#000000       gui=bold




 " (     
 " 
 " )



hi! link CursorColumn CursorLine

"Directory	directory names (and other special names in listings)
"DiffAdd		diff mode: Added line |diff.txt|
"DiffChange	diff mode: Changed line |diff.txt|
"DiffDelete	diff mode: Deleted line |diff.txt|
"DiffText	diff mode: Changed text within a changed line |diff.txt|
"ErrorMsg	error messages on the command line
"VertSplit	the column separating vertically split windows
"Folded		line used for closed folds
"FoldColumn	'foldcolumn'
"SignColumn	column where |signs| are displayed




"MoreMsg		|more-prompt|
hi NonText		guifg=#808080	guibg=#121212
hi Pmenu		guifg=#50aa40	guibg=#121212
hi PmenuSel		guifg=#ffffff	guibg=#121212
hi PmenuSbar	guibg=#121212
hi PmenuThumb	guibg=#808080  


"Question	|hit-enter| prompt and yes/no questions

"SpecialKey	Meta and special keys listed with ":map", also for text used
"SpellBad	Word that is not recognized by the spellchecker. |spell|
"SpellCap	Word that should start with a capital. |spell|
"SpellLocal	Word that is recognized by the spellchecker as one that is
"SpellRare	Word that is recognized by the spellchecker as one that is
"hi StatusLine	guifg=#808080	guibg=#121212
"StatusLineNC	status lines of not-current windows
"TabLine		tab pages line, not active tab page label
"TabLineFill	tab pages line, where there are no labels
"TabLineSel	tab pages line, active tab page label
"Title		titles for output from ":set all", ":autocmd" etc.
"VisualNOS	Visual mode selection when vim is "Not Owning the Selection".
"WarningMsg	warning messages
"WildMenu	current match in 'wildmenu' completion
"
"



hi! link Todo Comment

hi! link	Macro	Preproc
hi! link	Include	PreProc

hi! link	Statement	Keyword


hi! link	Special		Operator

